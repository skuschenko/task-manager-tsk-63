package com.tsc.skuschenko.tm.servlet.task;

import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.getRequestDispatcher("/WEB-INF/view/task-edit.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws  IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String status = req.getParameter("status");
        @NotNull final String description = req.getParameter("description");
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setStatus(status);
        task.setDescription(description);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
