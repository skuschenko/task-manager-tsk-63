package com.tsc.skuschenko.tm.servlet.project;

import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Project project =
                ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.getRequestDispatcher("/WEB-INF/view/project-edit.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws  IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String status = req.getParameter("status");
        @NotNull final String description = req.getParameter("description");
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setStatus(status);
        project.setDescription(description);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
