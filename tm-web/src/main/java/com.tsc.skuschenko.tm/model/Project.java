package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project {

    @Nullable
    private Date created = new Date();
    @Nullable
    private Date dateFinish;
    @Nullable
    private Date dateStart;
    @Nullable
    private String description = "";
    @NotNull
    private String id = UUID.randomUUID().toString();
    @Nullable
    private String name = "";

    @Nullable
    private String status = Status.NOT_STARTED.getDisplayName();

    public Project(@Nullable String name) {
        this.name = name;
    }

    public Project() {
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }
}
