package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Task;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Getter
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        save(new Task("t1"));
        save(new Task("t2"));
        save(new Task("t3"));
    }

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        save(new Task("t" + UUID.randomUUID().toString()));
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @NotNull
    public Task findById(@NotNull String id) {
        return tasks.get(id);
    }

    public void removeById(@Nullable String id) {
        tasks.remove(id);
    }

    public void save(@NotNull Task project) {
        tasks.put(project.getId(), project);
    }

}
