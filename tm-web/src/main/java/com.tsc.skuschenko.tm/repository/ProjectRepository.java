package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Project;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Getter
public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        save(new Project("p1"));
        save(new Project("p2"));
    }

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        save(new Project("p" + UUID.randomUUID().toString()));
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @NotNull
    public Project findById(@NotNull String id) {
        return projects.get(id);
    }

    public void removeById(@Nullable String id) {
        projects.remove(id);
    }

    public void save(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

}
