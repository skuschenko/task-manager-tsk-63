<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Task manager</title>
	</head>
	<style>
		h1 {
        font-size: 16px;
		}

		.table {
            border: 1px solid;
        }
        .table__header_tr__td {
            border: 1px solid;
            padding: 10px;
            width: 100%;

        }
        .table__header_tr {
            display: inline-flex;
            font-size: 25px;
            width: 100%;
            text-align: right;
        }
	</style>
	<body>
		<div class = "table">
			<div class = "table__header">
				<div class = "table__header_tr">
					<div class = "table__header_tr__td">
						Task manager
					</div>
					<div class = "table__header_tr__td">
						<a class="td__a" href="/projects">Projects</a>
						<a class="td__a" href="/tasks">Tasks</a>
					</div>
				</div>
			</div>
		</div>