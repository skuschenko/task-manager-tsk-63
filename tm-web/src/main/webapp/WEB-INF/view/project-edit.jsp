<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project edit</h1>

<form action="/project/edit/?id=${project.id}" method="post">
	<div class = "row row_1">
		<div class = "col col_1">
			<div class = "field__title">Name:</div>
			<div class = "field__value">
				<input type="text" name="name" value="${project.name}"/>
			</div>
		</div>
	</div>
	<div class = "row row_2">
		<div class = "col col_1">
			<div class = "field__title">Status:</div>
			<div class = "field__value">
				<input type="text" name="status" value="${project.status}"/>
			</div>
		</div>
	</div>
	<div class = "row row_3">
		<div class = "col col_1">
			<div class = "field__title">Description:</div>
			<div class = "field__value">
				<input type="text" name="description" value="${project.description}"/>
			</div>
		</div>
	</div>
	<div class = "row row_4">
		<div class = "col col_1">
			<button type="submit">Save project</button>
		</div>
	</div>
	<input type="hidden" name="id" value="${project.id}"/>
</form>

<jsp:include page="../include/_footer.jsp"/>
