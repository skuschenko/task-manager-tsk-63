<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task List</h1>

<div class = "table table__model">
	<div class = "table__header model__header">
		<div class = "table__header_tr model__header_tr">
			<div class = "table__header_tr__td model__header_tr__td">
				Id
			</div>
			<div class = "table__header_tr__td model__header_tr__td">
				Name
			</div>
			<div class = "table__header_tr__td model__header_tr__td">
				Status
			</div>
			<div class = "table__header_tr__td model__header_tr__td">
				Description
			</div>
			<div class = "table__header_tr__td model__header_tr__td">
                Date Start
            </div>
            <div class = "table__header_tr__td model__header_tr__td">
                Date Finish
            </div>
			<div class = "table__header_tr__td model__header_tr__td">
				Edit
			</div>
			<div class = "table__header_tr__td model__header_tr__td">
				Delete
			</div>
		</div>
	</div>
	<div class = "table__body model__body">
		<c:forEach var="task" items="${tasks}">
            <div class = "table__body_tr model__body_tr">
			<div class = "table__body_tr__td model__body_tr__td">
				<c:out value="${task.id}"/>
			</div>
			<div class = "table__body_tr__td model__body_tr__td">
				<c:out value="${task.name}"/>
			</div>
			<div class = "table__body_tr__td model__body_tr__td">
            	<c:out value="${task.status}"/>
            </div>
			<div class = "table__body_tr__td model__body_tr__td">
				<c:out value="${task.description}"/>
			</div>
			<div class = "table__body_tr__td model__body_tr__td">
                <c:out value="${task.dateStart}"/>
            </div>
            <div class = "table__body_tr__td model__body_tr__td">
                <c:out value="${task.dateFinish}"/>
            </div>
			<div class = "table__body_tr__td model__body_tr__td">
				<a href="/task/edit/?id=${task.id}">Edit</a>
			</div>
			<div class = "table__body_tr__td model__body_tr__td">
				<a href="/task/delete/?id=${task.id}">Delete</a>
			</div>
			</div>
		</c:forEach>
	</div>
</div>

<form action="/task/create" class = "model__form">
    <button>Create Task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>