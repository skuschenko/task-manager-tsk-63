package com.tsc.skuschenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public abstract class AbstractService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
