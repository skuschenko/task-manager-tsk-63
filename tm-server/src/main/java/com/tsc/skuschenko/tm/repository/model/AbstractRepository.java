package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.model.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbstractRepository<E extends AbstractEntity>
        extends JpaRepository<E, String> {

}
