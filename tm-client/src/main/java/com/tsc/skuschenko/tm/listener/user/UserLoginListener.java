package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.SessionEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class UserLoginListener extends AbstractUserListener {

    private static final String DESCRIPTION = "user login";

    private static final String NAME = "login";

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session =
                sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);
    }

    @Override
    public String name() {
        return NAME;
    }

}
